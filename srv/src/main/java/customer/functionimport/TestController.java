package customer.functionimport;

import com.sap.cloud.sdk.cloudplatform.connectivity.DestinationAccessor;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import customer.functionimport.vdm.namespaces.zbusfunimportsrv.BusFunctionImportFluentHelper;
import customer.functionimport.vdm.namespaces.zbusfunimportsrv.Complex_Bus;
import customer.functionimport.vdm.services.DefaultZBUSFUNIMPORTSRVService;
import customer.functionimport.vdm.services.ZBUSFUNIMPORTSRVService;

@RestController
@RequestMapping(path = "/api/v1/test", produces = "application/json")
public class TestController {

    @GetMapping(path = "/function-import")
    public Complex_Bus executeFunctionImport(){
        ZBUSFUNIMPORTSRVService service = new DefaultZBUSFUNIMPORTSRVService();
        BusFunctionImportFluentHelper helper = service.busFunctionImport("MH12AA1234");        
        Complex_Bus bus = helper.executeRequest(DestinationAccessor.getDestination("GW7").asHttp());        
        return bus;
    }
}
